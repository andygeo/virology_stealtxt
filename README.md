# .txt files thief
Реализация вредоносной программы, которая при запуске открывает стандартный текстовый редактор ОС и отображает в нем текст, параллельно подменяя все текстовые файлы в папке на такие же исполняемые. для четвертой лабораторной работы по курсу "Модели нарушения безопасности и вирусология" (осенний семестр 2018/2019 СПбГЭТУ).

студенты группы 4362

Георгица Андрей

Тищенко Виталия

* * *

Thief-encryptor of .txt files implementation for the fourth lab work of сomputer virology course (fall semester of 2018/2019 academic year, Saint-Petersburg ETU)

group №4362

Andrey Georgitsa

Tishchenko Vitaliya


